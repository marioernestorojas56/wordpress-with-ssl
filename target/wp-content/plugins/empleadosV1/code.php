<?php
/*
Plugin Name: Employee Registration Form
Description: A plugin to capture employee registration information and store it in the database.
Version: 1.0.34
Author: Fullstack Developer
*/

// Create the database table when the plugin is activated
function create_employee_registration_table()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "employee_registration";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE if not exists $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        employee_number varchar(55) NOT NULL,
        curp varchar(55) NOT NULL,
        file varchar(100) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'create_employee_registration_table');

// Add the shortcode for the employee registration form
function employee_registration_form_shortcode()
{
    ob_start();
    ?>
    <form action="" method="post" enctype="multipart/form-data">
        <div>
            <label for="employee_number">Número de empleado36:</label>
            <input type="text" id="employee_number" name="employee_number" required>
        </div>
        <div>
            <label for="curp">CURP:</label>
            <input type="text" id="curp" name="curp" required>
        </div>
        <div>
            <label for="file">Archivos:</label>
            <input type="file" id="file" name="file" multiple required>
        </div>
        <input type="submit" value="Submit" name="employee_registration_submit">
    </form>
    <?php
    if (isset($_POST['employee_registration_submit'])) {
        process_employee_registration();
    }
    return ob_get_clean();
}
add_shortcode('employee_registration_form', 'employee_registration_form_shortcode');

// Process the employee registration form data
function process_employee_registration()
{

    global $wpdb;

    // Recoge los datos del formulario
    $employee_number = sanitize_text_field($_POST['employee_number']);
    $curp = sanitize_text_field($_POST['curp']);
    $files =  $_FILES['file']['name'];

    // Prepara la inserción de los datos
    $table_name = $wpdb->prefix . "employee_registration";
    $data = array(
        'employee_number' => $employee_number,
        'curp' => $curp,
        'file' => $files
    );
    print_r($data);
    // Ejecuta la inserción
    $result = $wpdb->insert($table_name, $data);
    if ($result === false) {
        echo $table_name;
        echo '<div class="error">Ha ocurrido un error al insertar los datos.</div>';
       
        print_r($result);
    } else {
        echo '<div class="success">Los datos han sido insertados correctamente.</div>';
        //echo '<script type="text/javascript">location.reload();</script>';
    }
}

function search_employee_registration_func($atts)
{
    // Recoger los atributos opcionales del shortcode
    $atts = shortcode_atts(
        array(
            'title' => 'Buscar Registro de Empleado',
        ),
        $atts
    );
    // Comprobar si se ha enviado el formulario
    if (isset($_POST['search_employee_submit'])) {
        global $wpdb;
        $table_name = $wpdb->prefix . "employee_registration";

        // Recoger los valores de los campos
        $search_value = $_POST['search_value'];

        // Crear la consulta SQL para buscar el registro
        $query = $wpdb->prepare("SELECT * FROM $table_name WHERE employee_number = %s OR curp = %s", $search_value, $search_value);
        $result = $wpdb->get_results($query, ARRAY_A);

        // Mostrar los resultados
        if ($result) {
            $output = '<table class="search-result-table">';
            $output .= '<tr>';
            $output .= '<th>Número de Empleado</th>';
            $output .= '<th>CURP</th>';
            $output .= '<th>Archivos</th>';
            $output .= '</tr>';

            foreach ($result as $row) {
                $output .= '<tr>';
                $output .= '<td>' . $row['employee_number'] . '</td>';
                $output .= '<td>' . $row['curp'] . '</td>';
                $output .= '<td>' . $row['files'] . '</td>';
                $output .= '</tr>';
            }

            $output .= '</table>';
        } else {
            $output = '<p>No se encontraron resultados para la búsqueda</p>';
        }
    } else {
        // Mostrar el formulario
        $output = '<form method="post">';
        $output .= '<label>Número de Empleado o CURP:</label>';
        $output .= '<input type="text" name="search_value" required>';
        $output .= '<input type="submit" name="search_employee_submit" value="Buscar">';
        $output .= '</form>';
    }

    return $output;
}
add_shortcode('search_employee_registration', 'search_employee_registration_func');

function list_employee_registration_func()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "employee_registration";

    $query = $wpdb->prepare("SELECT * FROM $table_name ");
    $result = $wpdb->get_results($query, ARRAY_A);

    // Mostrar los resultados
    if ($result) {
        $output = '<table class="search-result-table">';
        $output .= '<tr>';
        $output .= '<th>Número de Empleado</th>';
        $output .= '<th>CURP</th>';
        $output .= '<th>Archivos</th>';
        $output .= '</tr>';

        foreach ($result as $row) {
            $output .= '<tr>';
            $output .= '<td>' . $row['employee_number'] . '</td>';
            $output .= '<td>' . $row['curp'] . '</td>';
            $output .= '<td>' . $row['files'] . '</td>';
            $output .= '</tr>';
        }

        $output .= '</table>';
    } else {
        $output = '<p>No se encontraron resultados para la búsqueda</p>';
    }
    return $output;

add_shortcode('list_employee_registration', 'list_employee_registration_func');

}
?>